package com.paytmmall.seller.pff.configs;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfig {
    @Value("${db.user}")
    private String dbUser;

    @Value("${db.password}")
    private String dbPassword;

    @Value("${db.maxConnections}")
    private int maxConnections;

    @Value("${db.initialConnections}")
    private int initialConnections;

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.driver}")
    private String dbDriver;


    @Value("${pool.setDefaultMaxPerRoute}")
    private Integer setDefaultMaxPerRoute;

    @Value("${pool.setMaxTotal}")
    private Integer setMaxTotal;


    @Bean
    public DataSource dataSource(){
        PoolProperties p = new PoolProperties();
        p.setUrl(dbUrl);
        p.setDriverClassName(dbDriver);
        p.setUsername(dbUser);
        p.setPassword(dbPassword);
        p.setMaxActive(maxConnections);
        p.setInitialSize(initialConnections);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);


        DataSource datasource = new DataSource();
        datasource.setPoolProperties(p);
        return datasource ;
    }


    @Bean
    public PoolingHttpClientConnectionManager getPoolingHttpClient() {
        PoolingHttpClientConnectionManager pool = new PoolingHttpClientConnectionManager();
        pool.setDefaultMaxPerRoute(setDefaultMaxPerRoute);
        pool.setMaxTotal(setMaxTotal);
        return pool;
    }

}