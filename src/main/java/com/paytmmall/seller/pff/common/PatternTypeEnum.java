package com.paytmmall.seller.pff.common;

public enum PatternTypeEnum {
    NUMBER,
    ALNUM,
    EMAIL
}
