package com.paytmmall.seller.pff.model;

import lombok.Data;

@Data
public class OrderRequest {

    private String tracking_number;
    private String manifest_id;

    private String order_ids;
    private String order_id;

    private String customer_email;
    private String placed_before;

    private String placed_after;
    private String before_id;

    private String before_oid;
    private String after_id;

    private String after_oid;
    private String status;

    private String phone;
    private String id;

    private String merchant_id;
    private String product_id;

    private String merchant_track_id;
    private String fulfillment_id;

    private String updated_after;
    private String updated_before;

    private String placed_with_in;
    private String limit;

    private String shipby_after;
    private String shipby_before;

    private String orn;
    private String product_ids;

    private String after_ff_service_id;
    private String before_ff_service_id;

    private String fulfillment_service;
    private String recharge_number;
}
