package com.paytmmall.seller.pff.controller;

import com.paytmmall.seller.pff.model.OrderRequest;
import com.paytmmall.seller.pff.utils.EsClientBuilder;
import com.paytmmall.seller.pff.utils.EsQueryBuilder;
import org.apache.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@EnableAutoConfiguration
public class ElasticSearchController {

    final static Logger logger = Logger.getLogger(ElasticSearchController.class);

    @Autowired
    EsClientBuilder esClientBuilder;

    @RequestMapping(value = "/es/index",
            method = GET)
    public void indexES() throws Exception {

        RestHighLevelClient client = esClientBuilder.getEsClient();

//        Map<String, Object> jsonMap = new HashMap<>();
//        jsonMap.put("user", "kimchy");
//        jsonMap.put("postDate", new Date());
//        jsonMap.put("message", "trying out Elasticsearch");
//        IndexRequest indexRequest = new IndexRequest("posts", "doc", "1")
//                .source(jsonMap);


        IndexRequest indexRequest = new IndexRequest(
                "orders",
                "doc",
                "1");
        String jsonToIndex = "{\"count\":1219326,\"data\":[{\"is_lmd\":0,\"warehouse_city\":\"new delhi\",\"created_at\":\"2018-04-13T18:29:59.000Z\",\"vertical_id\":66,\"discount\":0,\"merchant_id\":644590,\"fulfillment_service_id\":30,\"is_cod\":0,\"updated_at\":\"2018-04-13T18:30:00.000Z\",\"price\":0,\"product_id\":168143356,\"@version\":\"1\",\"tracking_number\":\"5311439674\",\"ship_by\":\"2018-04-13T18:30:01.000Z\",\"sku\":\"1300255\",\"delivered_at\":\"2018-04-13T18:30:01.000Z\",\"@index\":\"dashboard-2018-4-107\",\"item_id\":5311439674,\"merchant_track_id\":\"4969566568\",\"item_status\":7,\"merchant_payable\":0,\"fulfillment_id\":3661879126,\"tags\":[\"ex_dashboard_logstash\"],\"shipping_amount\":0,\"index_updated_at\":\"2018-04-14T23:07:31.793Z\",\"@timestamp\":\"2018-04-14T23:07:31.773Z\",\"parent_id\":null,\"warehouse_state\":\"Delhi\",\"shipper_id\":null,\"order_id\":4969566568,\"warehouse_id\":339209}]}";

        indexRequest.source(jsonToIndex, XContentType.JSON);

        indexRequest.timeout(TimeValue.timeValueSeconds(1));

        ActionListener<IndexResponse> listener = new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
                logger.info(indexResponse.toString());
            }

            @Override
            public void onFailure(Exception e) {

            }
        };

        client.indexAsync(indexRequest, listener);


    }

    @RequestMapping(value = "/es/search",
            method = GET)
    public void searchES() throws Exception {

        RestHighLevelClient client = esClientBuilder.getEsClient();
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setAfter_oid("4969566567");
        EsQueryBuilder esQueryBuilder = new EsQueryBuilder(orderRequest);

        SearchSourceBuilder searchSourceBuilder = esQueryBuilder.buildFilters().buildQuery().getSearchSourceBuilder();




        ActionListener<SearchResponse> listener = new ActionListener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse searchResponse) {
                SearchHits hits = searchResponse.getHits();
                SearchHit[] searchHits = hits.getHits();
                logger.info(searchResponse.toString());
                for (SearchHit hit : searchHits) {
                    Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                    String documentTitle = (String) sourceAsMap.get("title");
                    List<Object> users = (List<Object>) sourceAsMap.get("user");
                    Map<String, Object> innerObject = (Map<String, Object>) sourceAsMap.get("innerObject");
                }
            }

            @Override
            public void onFailure(Exception e) {

            }
        };


        SearchRequest searchRequest = new SearchRequest("orders");
        searchRequest.types("doc");
        searchRequest.source(searchSourceBuilder);

        client.searchAsync(searchRequest, listener);

    }
}
