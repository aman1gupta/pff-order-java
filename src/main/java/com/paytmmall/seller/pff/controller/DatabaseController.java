package com.paytmmall.seller.pff.controller;

import com.paytmmall.seller.pff.db.PopulateOrderData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@EnableAutoConfiguration
public class DatabaseController {


    final static Logger logger = Logger.getLogger(DatabaseController.class);

    @Autowired
    PopulateOrderData populateOrderData;

    @RequestMapping(value = "/testDb",
            method = GET)
    public void checkDb() throws Exception {
        logger.info(populateOrderData.testDatabase("select * from test.test"));

    }
}
