package com.paytmmall.seller.pff.controller;

import com.paytmmall.seller.pff.configs.Config;
import com.paytmmall.seller.pff.model.OrderRequest;
import com.paytmmall.seller.pff.utils.CallOmsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@EnableAutoConfiguration

@SpringBootApplication
@ImportResource({"classpath:applicationContext.xml", "Spring-Config.xml"})

public class Orders {

    final static Logger logger = Logger.getLogger(Orders.class);

    @Autowired
    Config config;

    @Autowired()
    @Qualifier("OMS")
    CallOmsService callOmsService;


    @Autowired
    private ApplicationContext appContext;
//
//    @Autowired
//    private OrderRequestValidator orderRequestValidator;
//
//    @Autowired
//    private OrderRequestModifier orderRequestModifier;

    @RequestMapping(value = "/v2/merchant/{merchant_id}/items_v2.json", method = GET, produces = "application/json")
    public DeferredResult<ResponseEntity> itemOrder(
            @PathVariable("merchant_id") long merchantId,
            @ModelAttribute OrderRequest orderRequest,
            HttpServletRequest httpServletRequest,
            BindingResult bindingResult) {

        callOmsService.setQueryString(httpServletRequest.getQueryString());
        callOmsService.setCookies(httpServletRequest.getCookies());
        callOmsService.setOrderRequest(orderRequest);
        callOmsService.setMerchantId(merchantId);
        DeferredResult<ResponseEntity> deferredResult = new DeferredResult<ResponseEntity>();

//        orderRequestValidator.validate(orderRequest, bindingResult);
//        orderRequestModifier.modify(orderRequest);

//        if (bindingResult.hasErrors()) {
//            List<ObjectError> errors = bindingResult.getAllErrors();
//            for(ObjectError error: errors){
//                throw new ValidationFailedException(error.getCode(), error.getDefaultMessage());
//            }
//        }

        ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) appContext.getBean("taskExecutor");
        CompletableFuture.supplyAsync(callOmsService, taskExecutor)
                .whenCompleteAsync((result, throwable) -> {
                    deferredResult.setResult(result);
                });

        return deferredResult;

    }

    @RequestMapping(value = "/health", method = GET)
    public HttpStatus health() {
        return HttpStatus.OK;
    }

    public static void main(String[] args) {
        SpringApplication.run(Orders.class, args);
    }

}
