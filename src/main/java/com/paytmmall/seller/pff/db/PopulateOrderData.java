package com.paytmmall.seller.pff.db;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PopulateOrderData {
    @Autowired
    DataSource dataSource;

    public String testDatabase(String sql){

        StringBuffer stringBuffer = new StringBuffer();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)){

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                stringBuffer.append(" Id= " + rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }
}
