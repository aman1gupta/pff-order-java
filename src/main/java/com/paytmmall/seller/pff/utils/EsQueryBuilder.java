package com.paytmmall.seller.pff.utils;

import com.paytmmall.seller.pff.model.OrderRequest;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.StringUtils.isNumeric;

public class EsQueryBuilder {
    EsQueryBuilder esQueryBuilder;
    OrderRequest orderRequest;
    SearchSourceBuilder searchSourceBuilder;

    public EsQueryBuilder(OrderRequest orderRequest) {
        this.orderRequest = orderRequest;
        searchSourceBuilder = new SearchSourceBuilder();
    }
//    var dQuery = {
//            'query': {'filtered': {'filter': {'bool': {'must': must } } } },
//            'size': pageSize,
//            'sort': { 'item_id': {'order': 'desc'}}
//};
//
//    if (filters.placed_before) must.push({'range': {'created_at': {'lt': moment(filters.placed_before).utc().toISOString() } } } );
//            if (filters.placed_after) must.push({'range': {'created_at': {'gt': moment(filters.placed_after).utc().toISOString() } } } );
//            if (filters.before_id) must.push({'range': {'order_id': {'lt': filters.before_id } } } );
//            if (filters.after_id) must.push({'range': {'order_id': {'gt': filters.after_id } } } );
//            if(req.query.after_ff_service_id)  must.push({'range': {'after_fulfillment_service_id': {'gt': req.query.after_ff_service_id } } } );
//            if(req.query.before_ff_service_id)  must.push({'range': {'before_fulfillment_service_id': {'lt': req.query.before_ff_service_id } } } );
//            if (filters.customer_email) must.push({'term': {'customer_email': filters.customer_email } } );
//            if (req.query.fulfillment_service) must.push({'term': {'fulfillment_service': req.query.fulfillment_service } } );
//            if (filters.recharge_number) dQuery.query.filtered.query = {'multi_match': {'query': filters.recharge_number, 'fields': ['item_name', 'ffreq_*'] }};
//            if (filters.IP) dQuery.query.filtered.query = {"range": {"remote_ip": {"gte": IPs.from,"lte": IPs.to}}};
//            delete req.query.customer_email;
//            delete req.query.recharge_number;
//            delete req.query.customer_ip;
//            delete req.query.mask;
//
//            var options = {
//            query: dQuery,
//            ignore_unavailable: true
//            };


    public EsQueryBuilder buildFilters() {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();


        if (StringUtils.isNotEmpty(orderRequest.getPlaced_after())) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("data.placed_after").gt(formatDate(orderRequest.getPlaced_after())));
        }

        if (StringUtils.isNotEmpty(orderRequest.getPlaced_before())) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("data.placed_before").lt(formatDate(orderRequest.getPlaced_before())));
        }

        if (StringUtils.isNotEmpty(orderRequest.getAfter_oid())) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("data.order_id").gt(orderRequest.getAfter_oid()));
        }

        if (StringUtils.isNotEmpty(orderRequest.getBefore_oid())) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("data.order_id").gt(orderRequest.getBefore_oid()));
        }

        if (StringUtils.isNotEmpty(orderRequest.getAfter_ff_service_id())) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("data.fulfillment_service_id").gt(orderRequest.getAfter_ff_service_id()));
        }

        if (StringUtils.isNotEmpty(orderRequest.getBefore_ff_service_id())) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("data.fulfillment_service_id").gt(orderRequest.getAfter_ff_service_id()));
        }


        if (StringUtils.isNotEmpty(orderRequest.getCustomer_email())) {
            boolQueryBuilder.must(QueryBuilders.matchQuery("data.cutomer_email", orderRequest.getCustomer_email()));
        }

        if (StringUtils.isNotEmpty(orderRequest.getFulfillment_service())) {
            boolQueryBuilder.must(QueryBuilders.matchQuery("data.fulfillment_service", orderRequest.getFulfillment_service()));
        }

        if (StringUtils.isNotEmpty(orderRequest.getRecharge_number())) {
            boolQueryBuilder.must(QueryBuilders.matchQuery("data.recharge_number", orderRequest.getRecharge_number()));
        }

        //TODO Ip filtering


        boolQueryBuilder.must(QueryBuilders.matchQuery("data.merchant_id", "644590"));

        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchSourceBuilder.sort("data.item_id", SortOrder.DESC);
        int pageSize = 10;
        if (orderRequest.getLimit() != null && isNumeric(orderRequest.getLimit())) {
            pageSize = Integer.valueOf(orderRequest.getLimit());
        }
        searchSourceBuilder.size(pageSize);
        searchSourceBuilder.query(boolQueryBuilder);
        return this;
    }

    public EsQueryBuilder buildQuery() {
        return this;
    }

    public SearchSourceBuilder getSearchSourceBuilder() {
        return searchSourceBuilder;
    }

    private String formatDate(String inputDate) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        return df.format(inputDate);
    }

}



