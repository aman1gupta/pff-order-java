package com.paytmmall.seller.pff.utils;

import com.paytmmall.seller.pff.model.OrderRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;

@Component
public class OrderRequestModifier {

    private Date date;

    @Value("${limits.daterange}")
    private Long dateRange;

    @Value("${limits.selects}")
    private Long selects;

    public void modify(OrderRequest orderRequest) {
        date = new Date();
        if (orderRequest.getPlaced_after() != null
                && Long.parseLong(orderRequest.getPlaced_after()) > date.getTime()) {
            orderRequest.setPlaced_after(Long.toString(date.getTime()));
        }
        if (orderRequest.getPlaced_before() != null
                && Long.parseLong(orderRequest.getPlaced_before()) > date.getTime()) {
            orderRequest.setPlaced_before(Long.toString(date.getTime()));
        }
        if (orderRequest.getUpdated_after() != null
                && Long.parseLong(orderRequest.getUpdated_after()) > date.getTime()) {
            orderRequest.setUpdated_after(Long.toString(date.getTime()));
        }
        if (orderRequest.getUpdated_before() != null
                && Long.parseLong(orderRequest.getUpdated_before()) > date.getTime()) {
            orderRequest.setUpdated_before(Long.toString(date.getTime()));
        }
        if (orderRequest.getPlaced_with_in() != null
                && orderRequest.getPlaced_after() == null
                && Long.parseLong(orderRequest.getPlaced_with_in()) > dateRange) {
            orderRequest.setPlaced_with_in(Long.toString(dateRange));
            orderRequest.setPlaced_after(Long.toString(date.getTime() - dateRange*24*3600000));
        }
        // enforce filter middleware from node
        if (orderRequest.getLimit() != null) {
            if(Long.parseLong(orderRequest.getLimit()) > selects) {
                orderRequest.setLimit(Long.toString(selects));
            }
        }

        Class cls = orderRequest.getClass();
        String[] dateFields = new String[]{"Placed_before", "Placed_after", "Shipby_before", "Shipby_after", "Updated_before", "Updated_after"};
        Method m;
        for (String field : dateFields) {
            try {
                m = cls.getDeclaredMethod("get" + field);
                m.invoke(null);
                m = cls.getDeclaredMethod("set" + field, String.class);
                m.invoke(date);
            } catch (Exception ex) {
                continue;
            }
        }

    }
}
