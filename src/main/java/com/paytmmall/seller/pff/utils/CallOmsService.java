package com.paytmmall.seller.pff.utils;

import com.paytmmall.seller.pff.model.OrderRequest;
import lombok.Data;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.util.function.Supplier;

@Data
@Service(value = "OMS")
public class CallOmsService implements Supplier<ResponseEntity> {

    final static Logger logger = Logger.getLogger(CallOmsService.class);

    @Value("${oms.orders.service.url}")
    private String url;

    @Value("${oms.orders.service.api}")
    private String api;

    @Value("${cookie.domain}")
    private String domain;

    @Value("${cookie.path}")
    private String path;


    private String queryString;

    private OrderRequest orderRequest;

    private Long merchantId;

    private Cookie[] cookies;

    @Autowired
    PoolingHttpClientConnectionManager pool;

    @Override
    public ResponseEntity get() {
        try {
            CookieStore cookieStore = new BasicCookieStore();
            BasicClientCookie basicClientCookie;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    basicClientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                    basicClientCookie.setDomain(domain);
                    basicClientCookie.setPath(path);
                    cookieStore.addCookie(basicClientCookie);
                }
            }

            CloseableHttpClient httpclient = HttpClients.custom()
                    .setDefaultCookieStore(cookieStore)
                    .build();

            StringBuilder redirectionUrl = new StringBuilder(url).append(merchantId).append(api).append("?").append(queryString);
            HttpGet httpGet = new HttpGet(redirectionUrl.toString());

            CloseableHttpResponse response = httpclient.execute(httpGet);

            return new ResponseEntity(EntityUtils.toString(response.getEntity(), "UTF-8"),
                    HttpStatus.valueOf(response.getStatusLine().getStatusCode()));

        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

}