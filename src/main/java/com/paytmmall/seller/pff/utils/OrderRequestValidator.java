package com.paytmmall.seller.pff.utils;

import com.paytmmall.seller.pff.common.PatternTypeEnum;
import com.paytmmall.seller.pff.model.OrderRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class OrderRequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == OrderRequest.class;
    }

    @Override
    public void validate(Object orderObject, Errors errors) {
        OrderRequest orderRequest = (OrderRequest) orderObject;
        if (orderRequest.getTracking_number() != null
                && !PatternMatcher.matches(orderRequest.getTracking_number(), PatternTypeEnum.ALNUM)) {
            errors.reject(HttpStatus.PRECONDITION_FAILED.toString(), "No Valid tracking_number sent!");
        }
        if (orderRequest.getManifest_id() != null
                && !PatternMatcher.matches(orderRequest.getManifest_id(), PatternTypeEnum.NUMBER)) {
            errors.reject(HttpStatus.PRECONDITION_FAILED.toString(), "No Valid manifest_id sent!");
        }
        if (orderRequest.getCustomer_email() != null
                && !PatternMatcher.matches(orderRequest.getCustomer_email(), PatternTypeEnum.ALNUM)) {
            errors.reject(HttpStatus.PRECONDITION_FAILED.toString(), "No Valid customer_email sent!");
        }

        // TODO: add more filters here

        if (orderRequest.getPlaced_with_in() != null
                && orderRequest.getPlaced_before() != null) {
            errors.reject(HttpStatus.BAD_REQUEST.toString(), "Placed with in filter cannot come with placed before.");
        }
    }
}
