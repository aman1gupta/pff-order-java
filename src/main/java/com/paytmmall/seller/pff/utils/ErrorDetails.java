package com.paytmmall.seller.pff.utils;

import org.springframework.stereotype.Component;

@Component
public class ErrorDetails {
    private String error;
    private String stack;

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public String getStack() {
        return stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }
}
