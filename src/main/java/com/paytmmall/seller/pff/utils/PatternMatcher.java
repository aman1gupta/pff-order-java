package com.paytmmall.seller.pff.utils;

import com.paytmmall.seller.pff.common.PatternTypeEnum;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Component
public class PatternMatcher {

    final static Logger logger = Logger.getLogger(PatternMatcher.class);

    private static boolean isNumber(String s) {
        return Pattern.matches("[0-9]+", s);
    }
    
    private static boolean isAlphaNum(String s) {
    	return Pattern.matches("[a-zA-Z0-9]+", s);
    }

    private static boolean isValidEmail(String s) {
        return Pattern.matches("([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+", s);
    }

    public static boolean matches(String s, PatternTypeEnum validatorEnum) {
        try {
            switch (validatorEnum) {
                case NUMBER: return isNumber(s);
                case ALNUM: return isAlphaNum(s);
                case EMAIL: return isValidEmail(s);
                default: return false;
            }

        } catch (PatternSyntaxException e) {
            logger.error("Invalid regex used");
            return false;
        }
    }
}




