package com.paytmmall.seller.pff.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class ValidationFailedException extends RuntimeException {

    private HttpStatus errorStatus;

    public ValidationFailedException(String errorCode, String message) {
        super(message);
        errorStatus = HttpStatus.resolve(Integer.parseInt(errorCode));
    }

    public HttpStatus getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(HttpStatus errorStatus) {
        this.errorStatus = errorStatus;
    }
}